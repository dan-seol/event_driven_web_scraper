import json
import time

from kafka import KafkaProducer
from bs4 import BeautifulSoup

import requests

# Kafka configuration
bootstrap_servers = ['localhost:29092']
topic = 'scraping-events'

# Web scraper configuration
country_name_url = 'https://www.scrapethissite.com/pages/simple/'
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}

# Create Kafka producer
producer = KafkaProducer(bootstrap_servers=bootstrap_servers,
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))

def scrape_country_name():
    # Make HTTP request to fetch the product page
    response = requests.get(country_name_url, headers=headers)
    
    # Parse the HTML response
    soup = BeautifulSoup(response.content, 'html.parser')
    
    country_name_tag = soup.find('h3', {'class': 'country-name'})
    # Extract the country_name from the parsed HTML
    while country_name_tag:
        country_name = country_name_tag.text.strip()
        # Create a scraping event message
        scraping_event = {
            'timestamp': int(time.time()),
            'country_name_url': country_name_url,
            'country_name': country_name
        }
        # Publish the scraping event to Kafka topic
        producer.send(topic, value=scraping_event)
        print('Scraping event published:', scraping_event)
        country_name_tag = country_name_tag.find_next('h3', {'class': 'country-name'})

    producer.flush()

# Run the scraper at regular intervals
while True:
    scrape_country_name()
    time.sleep(10)  
# Scrapes every 10 seconds
